const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
mongoose.connect(
  "mongodb://localhost:27017/PPLDatabase",
  { useNewUrlParser: true }
);

const timelineRoutes = require("./routes/timelineRoutes");
const routerRegisterForm = require("./routes/routerRegisterForm");

app.use("/", routerRegisterForm);
app.use("/timeline", timelineRoutes);
app.use(express.static("Uploads"));

app.listen(3050, () => console.log("<<<< PPL SERVER RUNNING >>>>"));
