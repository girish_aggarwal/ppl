const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  category: { type: String, require: true },
  fileName: { type: String, require: true }
});

module.exports = mongoose.model("categoryCollectionPPL", categorySchema);
