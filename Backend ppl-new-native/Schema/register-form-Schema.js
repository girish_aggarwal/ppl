const mongoose = require("mongoose");

const registerFormSchema = mongoose.Schema({
  username: { type: String, require: true },
  password: { type: String, require: true },
  email: { type: String, require: true },
  fname: { type: String, require: true },
  lname: { type: String, require: true },
  verify: { type: Boolean, default: false },
  otp: Number
});

module.exports = mongoose.model("formCollectionPPL", registerFormSchema);
