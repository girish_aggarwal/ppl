const mongoose = require("mongoose");

const uploadImageSchema = mongoose.Schema({
  email: { type: String, require: true },
  username: { type: String, require: true },
  title: String,
  fileName: { type: String, require: true },
  description: String,
  time: { type: Date },
  comments: { type: [], default: [] },
  likes: { type: [], default: [] }
});

module.exports = mongoose.model("imageCollectionPPL", uploadImageSchema);
