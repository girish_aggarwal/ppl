const express = require("express");
const router = express.Router();
const uploadAPI = require("../API's/uploadAPI");
const categoryAPI = require("../API's/categoryAPI");
const multer = require("multer");
const { get, set } = require("lodash");

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "./Uploads");
  },
  filename: function(req, file, cb) {
    cb(null, "img" + Date.now() + ".png");
  }
});
const upload = multer({ storage });

router.post("/uploadPost", upload.single("uploadedFile"), async function(
  req,
  res
) {
  try {
    req.body.fileName = req.file.filename;
    req.body.time = Date.now();
    let result = await uploadAPI.add(req.body);
    result = await uploadAPI.getAllPosts();
    result.reverse();
    res.send(result);
  } catch (err) {
    console.log("Error In Timeline Routes upload Post", err);
    res.status(404).send(err);
  }
});

router.post("/getPosts/:id", async function(req, res) {
  try {
    let result = [];
    if (req.params.id === "email") {
      result = await uploadAPI.getAllPosts({ email: req.body.email });
    } else if (req.params.id === "all") {
      result = await uploadAPI.getAllPosts();
    } else if (req.params.id === "single") {
      result = await uploadAPI.getOne(req.body);
    }
    result.reverse();
    res.send(result);
  } catch (err) {
    console.log("Error In Timeline Routes getPosts", err);
    res.status(404).send(err);
  }
});

router.post("/addComment", async function(req, res) {
  try {
    let result = await uploadAPI.updateComment(req.body);
    result = await uploadAPI.getAllComments(req.body);
    res.status(200).send({ comments: result[0].comments });
  } catch (err) {
    console.log("Error in addComment Router", err);
    res.status(404).send(err);
  }
});

router.post("/updateLikes", async function(req, res) {
  try {
    await uploadAPI.updateLikes({ _id: req.body._id }, req.body.likes);
    res.status(200).send("Updated Likes");
  } catch (err) {
    console.log("Error in update Likes Router", err);
    res.status(404).send(err);
  }
});

router.get("/getCategories", async function(req, res) {
  try {
    let result = await categoryAPI.get();
    res.send(result);
  } catch (error) {
    console.log("Error in getCategories Route", error);
    res.status(400).send(error);
  }
});
module.exports = router;
