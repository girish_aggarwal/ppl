const express = require("express");
const router = express.Router();
const userAPI = require("../API's/userAPI");
const nodemailer = require("nodemailer");
const _ = require("lodash");

function eval() {
  const x = Math.random();
  return x > 0.1 ? Math.floor(x * 1000) : eval();
}

const transporter = nodemailer.createTransport({
  service: "gmail",
  host: "smtp.gmail.com",
  auth: {
    user: "girish.pin2g@gmail.com",
    pass: "qwerty@12345"
  }
});

router.post("/mail/:ref", async function(req, res) {
  try {
    const match = await userAPI.get({ email: _.get(req.body, "email") });
    let text = "";
    const otp = eval();
    if (match.length === 0 && req.params.ref === "signup") {
      req.body.otp = otp;
      const result = await userAPI.add(req.body);
      res.send(
        `Dear ${
          result.username
        } verify your E-Mail through the OTP sent on your mail Id`
      );
      text = `<h1>HEY!! have you are now Signed Up with PPL...</h1>
              Here is your OTP >> "${
                result.otp
              }" . Don't Share this with anyone ...`;
    } else if (match.length === 1 && req.params.ref === "forgot") {
      const result = await userAPI.update(req.body, { otp });
      res.send(`Valid`);
      console.log("Forgot Password Request Recieved ..");
      text = `<h1>HEY!! you have requested to change Password...</h1> 
              Here is your OTP >> "${
                result.otp
              }" . Don't Share this with anyone ...`;
    } else {
      res.status(200).send("Invalid");
      return;
    }
    const mailoption = {
      from: "girish.pin2g@gmail.com",
      to: req.body.email,
      subject: "New Mail",
      html: text
    };

    transporter.sendMail(mailoption, function(err, info) {
      if (err) console.log("Error in send Mail>>>>>", err);
      else console.log("<<<<mail sent>>>>");
    });
  } catch (error) {
    res.status(404).send(error);
  }
});

router.post("/login", async function(req, res) {
  try {
    console.log("In login route >>> ", req.body);
    const match = await userAPI.get({ email: req.body.email });
    if (match.length > 0) {
      if (match[0].password === req.body.password) {
        if (match[0].verify === true) {
          res.status(200).send(["Correct", match[0].username]);
        } else res.status(200).send(["Email Not Verified"]);
      } else res.status(200).send(["Incorrect Password"]);
    } else {
      res.status(200).send(["Incorrect Email"]);
    }
  } catch (error) {
    res.status(404).send(error);
  }
});

router.post("/reset", async function(req, res) {
  try {
    const result = await userAPI.update({ email: req.body.email }, req.body);
    console.log("Password Changed SuccessFully", result);
    if (_.get(result, "n") === 1) res.send("Changed");
    else res.send("No Change");
  } catch (err) {
    console.log("error in reset rout", err);
    res.status(404).send(err);
  }
});

router.post("/otp", async function(req, res) {
  try {
    const result = await userAPI.get({
      email: req.body.email,
      otp: req.body.otp
    });
    if (_.isEmpty(result)) {
      res.status(200).send("");
    } else {
      if (req.body.identifier === "signup") {
        await userAPI.update({ email: req.body.email });
      }
      res.status(200).send("Correct");
    }
  } catch (error) {
    console.log("error in otp route", error);
    res.status(404).send(error);
  }
});

module.exports = router;
