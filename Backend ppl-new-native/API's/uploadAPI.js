const db = require("../Schema/upload-Image-Schema");

module.exports = {
  add: function(data) {
    return new Promise((resolve, reject) => {
      db.create(data, (error, result) => {
        if (error) {
          console.log("... ERROR In Upload Image API ...");
          reject(error);
        } else {
          console.log("... New Image Added .. in API ... result >> ");
          resolve(result);
        }
      });
    });
  },

  updateComment: function(data) {
    return new Promise((resolve, reject) => {
      db.update(
        { _id: data["_id"] },
        { $push: { comments: data.comment } },
        (error, result) => {
          if (error) {
            console.log("... ERROR In update API ...");
            reject(error);
          } else {
            console.log(" Updated Comments ");
            resolve(result);
          }
        }
      );
    });
  },

  getAllPosts: function(data) {
    if (typeof data === "object") {
      return new Promise((resolve, reject) => {
        db.find(data, (error, result) => {
          if (error) {
            console.log("... ERROR In get All Posts ...");
            reject(error);
          } else {
            console.log(" Gave Email Posts ");
            resolve(result);
          }
        });
      });
    } else {
      return new Promise((resolve, reject) => {
        db.find({}, (error, result) => {
          if (error) {
            console.log("... ERROR In get All Posts ...");
            reject(error);
          } else {
            console.log(" Gave Posts ");
            resolve(result);
          }
        });
      });
    }
  },

  getOne: function(data) {
    return new Promise((resolve, reject) => {
      db.findOne(data, (error, result) => {
        if (error) {
          console.log("... ERROR In get One Posts ...");
          reject(error);
        } else {
          resolve(result);
        }
      });
    });
  },

  getAllComments: function(data) {
    return new Promise((resolve, reject) => {
      db.find({ _id: data["_id"] }, (error, result) => {
        if (error) {
          console.log("... ERROR In get All Posts ...");
          reject(error);
        } else {
          console.log(" Gave Comments ");
          resolve(result);
        }
      });
    });
  },

  updateLikes: function(data, array) {
    return new Promise((resolve, reject) => {
      db.update(data, { $set: { likes: [...array] } }, (error, result) => {
        if (error) {
          console.log("... ERROR In updateLikes API ...");
          reject(error);
        } else {
          console.log(" updated Likes ");
          resolve(result);
        }
      });
    });
  }
};
