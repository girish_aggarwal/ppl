const db = require("../Schema/register-form-Schema");
const _ = require("lodash");

module.exports = {
  add: function(data) {
    return new Promise((resolve, reject) => {
      db.create(data, (error, result) => {
        if (error) {
          console.log("... ERROR In Add USER API ...");
          reject(error);
        } else {
          resolve(result);
        }
      });
    });
  },

  get: function(data) {
    return new Promise((resolve, reject) => {
      db.find(data, (error, result) => {
        if (error) {
          console.log("... ERROR In Get USER API ...");
          reject(error);
        } else {
          resolve(result);
        }
      });
    });
  },

  update: function(data, toUpdate) {
    return new Promise((resolve, reject) => {
      db.update(
        data,
        typeof toUpdate === "object"
          ? _.hasIn(toUpdate, "otp")
            ? { $set: { otp: toUpdate.otp } }
            : { $set: { password: _.get(toUpdate, "password") } }
          : { $set: { verify: true } },
        (error, result) => {
          if (error) {
            console.log("... ERROR In Update USER API ... ");
            reject(error);
          } else {
            resolve(result);
          }
        }
      );
      // } else {
      //   db.update(data, { $set: { verify: true } }, (error, result) => {
      //     if (error) {
      //       console.log("... ERROR In Update USER API ... ");
      //       reject(error);
      //     } else {
      //       console.log("setted verify true");
      //       resolve(result);
      //     }
      //   });
      // }
    });
  }
};
