const db = require("../Schema/category-Schema");

module.exports = {
  add: function(data) {
    return new Promise((resolve, reject) => {
      db.create(data, (error, result) => {
        if (error) {
          console.log("Error in category API...");
          reject(error);
        } else {
          console.log("category added in API...");
          resolve(result);
        }
      });
    });
  },

  get: function(data) {
    return new Promise((resolve, reject) => {
      db.find({}, (error, result) => {
        if (error) reject(error);
        else resolve(result);
      });
    });
  }
};
