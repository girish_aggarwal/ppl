import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { createStackNavigator } from "react-navigation";
import LoginSignup from "./src/LoginSignup";
import Main from "./src/Home/Main";

const NavigationApp = createStackNavigator(
  {
    Login: { screen: LoginSignup },
    Main: { screen: Main }
  },
  { headerMode: "screen" }
);

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container} id="root">
        <NavigationApp />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center",
    margin: 5
  }
});
