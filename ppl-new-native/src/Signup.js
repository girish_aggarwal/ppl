import React, { Component } from "react";
import { Alert, TextInput, Button, View, Text, StyleSheet } from "react-native";
import axios from "axios";
import { forEach, isEmpty, get, set, unset } from "lodash";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        email: "",
        fname: "",
        lname: "",
        password: "",
        username: ""
      },
      error: {}
    };
  }

  validateTextfield = fieldName => {
    const { error, form } = this.state;
    const value = get(form, fieldName);

    let valid = false;

    if (fieldName === "email") {
      valid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    } else if (fieldName === "password") {
      valid = value.length >= 6 && value.split(" ").length === 1;
    } else {
      valid = !isEmpty(value);
    }

    if (valid) {
      unset(error, fieldName);
    } else {
      set(error, fieldName, `Invalid ${fieldName}`);
    }
    this.setState(this.state);
  };

  validateForm = () => {
    const { error, form } = this.state;
    forEach(form, (value, fieldName) => {
      this.validateTextfield(fieldName);
    });
    return isEmpty(error);
  };

  handleOnPress = e => {
    e.preventDefault();
    if (this.validateForm()) {
      axios
        .post(
          "http://192.168.100.149:3050/mail/signup",
          get(this.state, "form")
        )
        .then(response => {
          return response.data;
        })
        .then(data => {
          const { error, form } = this.state;
          if (data === "Invalid") {
            set(error, "email", "Email Already Registered");
            this.setState(this.state);
          } else {
            const { navigate } = this.props.navigation;
            navigate("OTP", {
              data,
              email: get(form, "email"),
              identifier: "signup"
            });
          }
        })
        .catch(err => {
          console.log("error in fetch >>", err);
        });
    }
  };

  onChange = (fieldName, value) => {
    const { form } = this.state;
    set(form, fieldName, value);
    this.validateTextfield(fieldName);
    this.setState(this.state);
  };

  showError = fieldName => {
    const { error } = this.state;
    const err = get(error, fieldName);
    if (err) {
      return <Text style={{ color: "red" }}>{err}</Text>;
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View id="signup-form" style={styles.main}>
        <Text style={styles.titleText}>Create An Account</Text>
        <View
          style={{
            borderBottomColor: "#f47b13",
            borderBottomWidth: 1
          }}
        />
        <TextInput
          style={{ height: 40 }}
          placeholder="First Name"
          onChangeText={text => this.onChange("fname", text)}
        />
        {this.showError("fname")}
        <TextInput
          style={{ height: 40 }}
          placeholder="Last Name"
          onChangeText={text => this.onChange("lname", text)}
        />
        {this.showError("lname")}
        <TextInput
          style={{ height: 40 }}
          placeholder="Username"
          onChangeText={text => this.onChange("username", text)}
        />
        {this.showError("username")}
        <TextInput
          style={{ height: 40 }}
          placeholder="Email"
          onChangeText={text => this.onChange("email", text)}
        />
        {this.showError("email")}
        <TextInput
          style={{ height: 40 }}
          type="password"
          secureTextEntry={true}
          placeholder="Password"
          maxLength={10}
          onChangeText={text => this.onChange("password", text)}
        />
        {this.showError("password")}
        <View style={styles.button}>
          <Button onPress={this.handleOnPress} title="Register" />
        </View>
        <Text>
          I already have an account.<Text
            style={{ color: "blue" }}
            onPress={() => {
              navigate("Login");
            }}
          >
            Login My Account !
          </Text>
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fbf9f9",
    flex: 2,
    justifyContent: "space-around"
  },
  titleText: {
    color: "#f47b13",
    fontSize: 30,
    fontWeight: "bold"
  },
  button: {
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

export default Signup;
