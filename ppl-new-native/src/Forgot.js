import React, { Component } from "react";
import { Alert, TextInput, Button, View, Text, StyleSheet } from "react-native";
import axios from "axios";
import { set, get, unset, isEmpty } from "lodash";

class Forgot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        email: ""
      },
      error: {}
    };
  }

  validateEmail = () => {
    const { error, form } = this.state;
    const value = get(form, "email");
    const valid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    if (valid) unset(error, "email");
    else set(error, "email", "Invalid Email");
    this.setState(this.state);
  };

  handleOnPress = e => {
    e.preventDefault();
    this.validateEmail();
    const { error } = this.state;
    if (isEmpty(error)) {
      axios
        .post(
          "http://192.168.100.149:3050/mail/forgot",
          get(this.state, "form")
        )
        .then(response => {
          return response.data;
        })
        .then(data => {
          const { navigate } = this.props.navigation;
          const { form } = this.state;
          if (data === "Valid") {
            const text =
              "To Verify ...Please, find the OTP sent on your registered mail-id";
            navigate("OTP", {
              data: text,
              email: get(form, "email"),
              identifier: "forgot"
            });
          } else {
            set(error, "email", "Email Not Registered");
            this.setState(this.state);
          }
        })
        .catch(err => {
          console.log("error in fetch >>", err);
        });
    }
  };

  showError = () => {
    const { error } = this.state;
    const err = get(error, "email");
    if (err) {
      return <Text style={{ color: "red" }}>{err}</Text>;
    }
  };

  onChange = value => {
    const { form } = this.state;
    set(form, "email", value);
    this.validateEmail();
    this.setState(this.state);
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View id="1" style={styles.main}>
        <View>
          <Text style={styles.titleText}>Forgot Password</Text>
          <View
            style={{
              borderBottomColor: "#f47b13",
              borderBottomWidth: 2
            }}
          />
        </View>
        <View>
          <TextInput
            style={{ height: 40 }}
            placeholder="Email"
            onChangeText={text => this.onChange(text)}
          />
          {this.showError()}
          <View
            style={{
              margin: 10,
              flexDirection: "row",
              alignContent: "space-around",
              justifyContent: "space-around"
            }}
          >
            <Button onPress={this.handleOnPress} title="Submit" />
          </View>
        </View>
        <View>
          <Text>I do not have any account yet.</Text>

          <Text style={{ color: "blue" }} onPress={() => navigate("Signup")}>
            Create My Account Now !
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fbf9f9",
    flex: 2,
    justifyContent: "space-around"
  },
  titleText: {
    color: "#f47b13",
    fontSize: 30,
    fontWeight: "bold"
  },
  button: {
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

export default Forgot;
