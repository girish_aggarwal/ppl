import React, { Component } from "react";
import { Alert, TextInput, Button, View, Text, StyleSheet } from "react-native";
import axios from "axios";
import { set, get, unset, isEmpty } from "lodash";

class OTP extends Component {
  constructor(props) {
    super(props);
    const { email, identifier, data } = get(props, "navigation.state.params");
    this.state = {
      form: {
        otp: 0,
        email,
        identifier,
        data
      },
      error: {}
    };
  }

  validateOtp = () => {
    const { error, form } = this.state;
    const value = get(form, "otp");
    const valid = value.length === 3;
    if (valid) unset(error, "otp");
    else set(error, "otp", "Invalid OTP");
    this.setState(this.state);
  };

  handleOnPress = e => {
    e.preventDefault();
    this.validateOtp();
    const { error } = this.state;
    if (isEmpty(error)) {
      const { navigate } = this.props.navigation;
      axios
        .post("http://192.168.100.149:3050/otp", get(this.state, "form"))
        .then(response => {
          return response.data;
        })
        .then(data => {
          if (data === "Correct") {
            const { identifier, email } = get(this.state, "form");
            if (identifier === "signup") {
              Alert.alert("Success", "Login Again to continue ...");
              navigate("Login");
            } else {
              Alert.alert("Success", "Reset Your Password ...");
              navigate("Reset", { email });
            }
          } else {
            set(error, "otp", "Invalid OTP");
            this.setState(this.state);
          }
        })
        .catch(err => {
          console.log("error in otp fetch >>", err);
        });
    }
  };

  showError = () => {
    const { error } = this.state;
    const err = get(error, "otp");
    if (err) {
      return <Text style={{ color: "red" }}>{err}</Text>;
    }
  };

  onChange = value => {
    const { form } = this.state;
    set(form, "otp", value);
    this.validateOtp();
    this.setState(this.state);
  };

  render() {
    return (
      <View id="1" style={styles.main}>
        <View>
          <Text style={styles.titleText}>One Time Password (OTP)</Text>
          <View
            style={{
              borderBottomColor: "#f47b13",
              borderBottomWidth: 2
            }}
          />
        </View>
        <View>
          <Text style={styles.text}>{get(this.state, "form.data")}</Text>
        </View>
        <View style={{ flex: 3, justifyContent: "center" }}>
          <TextInput
            style={{ height: 40 }}
            placeholder="one time password"
            onChangeText={text => this.onChange(text)}
          />
          {this.showError()}
          <View
            style={{
              margin: 10,
              flexDirection: "row",
              alignContent: "space-around",
              justifyContent: "space-around"
            }}
          >
            <Button onPress={this.handleOnPress} title="Submit" />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fbf9f9",
    flex: 2,
    justifyContent: "space-around"
  },
  titleText: {
    color: "#f47b13",
    fontSize: 30,
    fontWeight: "bold"
  },
  text: {
    color: "#fb4",
    fontSize: 15
  },
  button: {
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

export default OTP;
