import React, { Component } from "react";
import {
  AsyncStorage,
  TextInput,
  Button,
  View,
  Text,
  StyleSheet
} from "react-native";
import axios from "axios";
import { set, get, unset, forEach, isEmpty } from "lodash";
import Header from "./Header";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        email: "",
        password: ""
      },
      error: {}
    };
  }

  validateTextField = fieldName => {
    const { error, form } = this.state;
    const value = get(form, fieldName);
    let valid = false;

    if (fieldName === "email") {
      valid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
    } else {
      valid = value.length >= 6 && value.split(" ").length === 1;
    }

    if (valid) {
      unset(error, fieldName);
    } else {
      set(error, fieldName, `Invalid ${fieldName}`);
    }
    this.setState(this.state);
  };

  validateForm = () => {
    const { error, form } = this.state;

    forEach(form, (value, fieldName) => {
      this.validateTextField(fieldName);
    });
    return isEmpty(error);
  };

  storeData = async username => {
    try {
      await AsyncStorage.setItem("email", get(this.state, "form.email"));
      await AsyncStorage.setItem("username", username);
    } catch (error) {
      console.log("error in login Async Storage", error);
    }
  };

  handleOnPress = e => {
    e.preventDefault();
    const { error } = this.state;
    unset(error, "backendError");
    if (this.validateForm()) {
      axios
        .post("http://192.168.100.149:3050/login", get(this.state, "form"))
        .then(response => {
          return response.data;
        })
        .then(data => {
          if (data[0] === "Correct") {
            const { navigate } = this.props.screenProps.root;
            this.storeData(data[1]);
            navigate("Main", {
              email: get(this.state, "form.email"),
              username: data[1]
            });
          } else {
            set(error, "backendError", `${data}`);
            this.setState(this.state);
          }
        })
        .catch(err => {
          console.log("error in fetch >>", err);
        });
    }
  };

  onChange = (fieldName, value) => {
    const { form } = this.state;
    set(form, fieldName, value);
    this.validateTextField(fieldName);
    this.setState(this.state);
  };

  showError = fieldName => {
    const { error } = this.state;
    let err = get(error, fieldName);
    if (err) {
      return <Text style={{ color: "red" }}>{err}</Text>;
    }
  };

  static navigationOptions = Header;

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View id="1" style={styles.main}>
        <View>
          <Text style={styles.titleText}>Log In</Text>
          <View
            style={{
              borderBottomColor: "#f47b13",
              borderBottomWidth: 2
            }}
          />
        </View>
        <View>
          {/* <Text>E- Mail</Text> */}
          <TextInput
            style={{ height: 40 }}
            placeholder="Email"
            onChangeText={text => this.onChange("email", text)}
          />
          {this.showError("email")}
          {/* <Text>Password</Text> */}
          <TextInput
            style={{ height: 40 }}
            placeholder="Password"
            secureTextEntry={true}
            onChangeText={text => this.onChange("password", text)}
          />
          {this.showError("password")}
          {this.showError("backendError")}
          <View
            style={{
              margin: 10,
              flexDirection: "row",
              alignContent: "space-around",
              justifyContent: "space-around"
            }}
          >
            <Button onPress={this.handleOnPress} title="Log In" />
            <Button
              onPress={() => navigate("Forgot")}
              title="Forgot Password"
            />
          </View>
        </View>
        <View>
          <Text>I do not have any account yet.</Text>

          <Text style={{ color: "blue" }} onPress={() => navigate("Signup")}>
            Create My Account Now !
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fbf9f9",
    flex: 2,
    justifyContent: "space-around"
  },
  titleText: {
    color: "#f47b13",
    fontSize: 30,
    fontWeight: "bold"
  },
  button: {
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

export default Login;
