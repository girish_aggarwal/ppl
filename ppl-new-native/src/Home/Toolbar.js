import React from "react";
import { Button, StyleSheet, ToolbarAndroid, Text } from "react-native";

export default class Toolbar extends React.Component {
  render() {
    return (
      <ToolbarAndroid style={styles.bar} titleColor="white">
        <Text>Welcome From PPL</Text>
      </ToolbarAndroid>
    );
  }
}

const styles = StyleSheet.create({
  bar: {
    flexDirection: "column",
    height: 56,
    backgroundColor: "#f4511e",
    justifyContent: "space-between"
  },
  left: {
    width: "20"
  }
});
