import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import Post from "./Post";
import { get, map } from "lodash";

export default class Content extends React.Component {
  render() {
    return (
      <View>
        {map(get(this.props, "posts"), (element, index) => {
          return (
            <Post
              key={index}
              post={element}
              loggedMail={get(this.props, "email")}
            />
          );
        })}
      </View>
    );
  }
}
