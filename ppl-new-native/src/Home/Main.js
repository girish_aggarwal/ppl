import React, { Component } from "react";
import { AsyncStorage, StatusBar, View } from "react-native";
import Timeline from "./Timeline";
import Toolbar from "./Toolbar";
import { get } from "lodash";

export default class Main extends Component {
  constructor(props) {
    super(props);
    const { email, username } = get(props, "navigation.state.params") || {
      email: "samshayans@gmail.co",
      username: "Sam Shayans"
    };
    this.state = {
      email,
      username
    };
  }

  retrieveData = async () => {
    try {
      const m = await AsyncStorage.getItem("email");
      const u = await AsyncStorage.getItem("username");

      // console.log("In Main >>> ", u, m);
    } catch (error) {
      console.log("error in retrieve AsyncStorage in Main ", error);
    }
  };

  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View>
        <StatusBar hidden={true} />
        <Toolbar />
        <Timeline parentState={this.state} />
        {this.retrieveData()}
      </View>
    );
  }
}
