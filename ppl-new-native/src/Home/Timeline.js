import React, { Component } from "react";
import { Text, View, StyleSheet, ScrollView } from "react-native";
import { set, get } from "lodash";
import Footer from "../Footer";
import TimelineView from "./TimelineView";
import Content from "./Content";
import UploadPost from "./UploadPost";
import axios from "axios";

export default class Timeline extends Component {
  constructor(props) {
    super(props);
    const { email, username } = get(this.props, "parentState");
    this.state = {
      uploadToggle: false,
      data: {
        email,
        username
      },
      posts: []
    };
  }

  componentDidMount() {
    axios
      .post("http://192.168.100.149:3050/timeline/getPosts/all")
      .then(response => response.data)
      .then(data => {
        this.setState({ posts: data });
      })
      .catch(error => console.log("Error in timeline axios >> ", error));
  }

  giveAllPosts = posts => {
    this.setState({
      posts
    });
  };

  onUploadToggle = () => {
    let toggle = !get(this.state, "uploadToggle");
    set(this.state, "uploadToggle", toggle);
    this.setState(this.state);
  };

  render() {
    return (
      <ScrollView keyboardShouldPersistTaps={"always"}>
        <TimelineView
          uploadToggle={this.onUploadToggle}
          data={get(this.state, "data")}
        />
        {get(this.state, "uploadToggle") && (
          <UploadPost
            data={get(this.state, "data")}
            givePosts={this.giveAllPosts}
            uploadToggle={this.onUploadToggle}
          />
        )}
        <View style={styles.container}>
          <Content
            posts={get(this.state, "posts")}
            email={get(this.state, "data.email")}
          />
        </View>
        <Footer style={{ justifySelf: "flex-end" }} />
        <Text>.</Text>
        <Text>.</Text>
        <Text>.</Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  main: {
    alignItems: "flex-start",
    backgroundColor: "white",
    margin: 10
  },
  img: { height: 200, width: "95%" }
});
