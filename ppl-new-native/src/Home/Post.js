import React, { Component } from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import PostBottom from "./PostBottom";
import { set, get } from "lodash";

export default class Post extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: {},
      time: new Date(),
      loggedMail: ""
    };
  }
  static getDerivedStateFromProps(nextProps) {
    const { loggedMail, post } = nextProps;
    return {
      loggedMail,
      post,
      time: new Date(get(post, "time"))
    };
  }
  render() {
    return (
      <View style={styles.main}>
        <Text style={styles.title}>{get(this.props, "post.title")}</Text>
        <Text style={{ fontSize: 10, color: "orange" }}>
          {get(this.props, "post.description")}
        </Text>
        <View style={{ width: "100%" }}>
          <View style={styles.user}>
            <Image
              style={styles.userImage}
              source={require("../../public/images/img_6.png")}
            />
            <Text>{get(this.props, "post.username")}</Text>
          </View>
          <View style={styles.time}>
            <Text>
              : Date > {this.state.time.toLocaleDateString()}
              : Time > {this.state.time.toLocaleTimeString()}
            </Text>
          </View>
        </View>
        <View style={styles.img}>
          <Image
            style={styles.img}
            source={{
              uri: `http://192.168.100.149:3050/${this.props.post.fileName}`
            }}
          />
        </View>
        <PostBottom
          post={get(this.state, "post")}
          loggedMail={get(this.state, "loggedMail")}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    alignItems: "flex-start",
    backgroundColor: "white",
    margin: 5,
    padding: 5,
    borderWidth: 2,
    borderColor: "#F44336"
  },
  title: {
    fontSize: 20,
    color: "#f47b13"
  },
  userImage: {
    height: 30,
    width: 30
  },
  img: {
    height: 200,
    width: "100%",
    borderTopWidth: 2
  },
  time: {
    alignSelf: "flex-end"
  },
  user: {
    alignSelf: "flex-start"
  }
});
