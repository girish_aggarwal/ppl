import React from "react";
import { Image, View, StyleSheet, Text, Button } from "react-native";
import { get } from "lodash";

export default class TimelineView extends React.Component {
  render() {
    return (
      <View style={styles.main}>
        <View>
          <Image
            style={styles.img}
            source={require("../../public/images/timeline_img1.png")}
          />
        </View>
        <Text>Name : {get(this.props, "data.username")}</Text>
        <View
          style={{
            alignSelf: "flex-end"
          }}
        >
          <Button
            title="Upload Post"
            onPress={e => {
              e.preventDefault();
              this.props.uploadToggle();
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    alignItems: "flex-start",
    margin: 10,
    backgroundColor: "#FAD7A0",
    borderWidth: 2,
    borderColor: "#F44336"
  },
  img: {
    borderRadius: 30,
    height: 50,
    width: 50,
    backgroundColor: "#D3D3D3"
  }
});
