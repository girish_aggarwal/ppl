import React, { Component } from "react";
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  View,
  TextInput,
  Text
} from "react-native";
import ImagePicker from "react-native-image-picker";
import { isEmpty, get, unset, set } from "lodash";
import axios from "axios";

const options = {
  title: "Select Avatar",
  storageOptions: {
    skipBackup: true,
    path: "images"
  }
};

export default class UploadPost extends Component {
  constructor(props) {
    super(props);
    const { email, username } = get(this.props, "data");
    this.state = {
      form: {
        image: {},
        email,
        username,
        title: "",
        description: ""
      },
      error: {}
    };
  }

  onUploadImagePress = e => {
    e.preventDefault();
    const { error } = this.state;
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log("User cancelled image picker", response);
        set(error, "image", "Image is necessary");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
        set(error, "image", "Error Occured .. Try Again");
      } else {
        const { form } = this.state;
        set(form, "image.name", response.fileName);
        set(form, "image.type", response.type);
        set(form, "image.uri", response.uri);
        unset(error, "image");
      }
      this.setState(this.state);
    });
  };

  showImage = () => {
    let { image } = get(this.state, "form");
    if (image) {
      return <Image source={image} style={styles.uploadAvatar} />;
    }
  };

  validateTitle = () => {
    const { form, error } = this.state;
    let value = get(form, "title");
    if (value) {
      unset(error, "title");
    } else {
      set(error, "title", "Can't be Blank");
    }
    this.setState(this.state);
  };

  validateImage = () => {
    const { form, error } = this.state;
    const image = get(form, "image");
    isEmpty(image)
      ? set(error, "image", "Image Can't be left null")
      : unset(error, "image");
    this.setState(this.state);
  };

  handleOnChange = (fieldname, value) => {
    const { form } = this.state;
    set(form, fieldname, value);
    if (fieldname === "title") {
      this.validateTitle("title");
    }
    this.setState(this.state);
  };

  showError = fieldname => {
    const { error } = this.state;
    let err = get(error, fieldname);
    if (err) {
      return <Text style={{ color: "red" }}>{err}</Text>;
    }
  };

  onSubmit = e => {
    e.preventDefault();
    const { form, error } = this.state;
    this.validateTitle();
    this.validateImage();
    if (isEmpty(error)) {
      let formData = new FormData();
      formData.append("uploadedFile", get(form, "image"));
      formData.append("username", form.username);
      formData.append("title", form.title);
      formData.append("email", form.email);
      formData.append("description", form.description);

      axios
        .post("http://192.168.100.149:3050/timeline/uploadPost", formData, {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data"
          }
        })
        .then(response => {
          return response.data;
        })
        .then(data => {
          this.props.givePosts(data);
          this.props.uploadToggle();
        })
        .catch(err => {
          console.log("Error in Catch", err);
        });
    }
  };

  render() {
    return (
      <View style={styles.main}>
        <Text style={styles.heading}>Upload Post</Text>
        <View
          style={{
            borderBottomColor: "#f47b13",
            borderBottomWidth: 2
          }}
        />
        <Text>
          Title <Text style={{ color: "red" }}>*</Text> :
        </Text>
        <TextInput
          style={{ height: 40 }}
          placeholder="Title"
          onChangeText={text => this.handleOnChange("title", text)}
        />
        {this.showError("title")}
        <Text>Description :</Text>
        <TextInput
          style={{ height: 40 }}
          placeholder="Description"
          onChangeText={text => this.handleOnChange("description", text)}
        />
        <TouchableOpacity>
          <Text
            onPress={this.onUploadImagePress}
            style={{
              padding: 5,
              backgroundColor: "#f47b13",
              width: "35%",
              color: "white"
            }}
          >
            Upload Image <Text style={{ color: "white" }}>*</Text>
          </Text>
        </TouchableOpacity>
        {this.showImage()}
        {this.showError("image")}
        <Text style={styles.submit} onPress={this.onSubmit}>
          Submit ->
        </Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  main: {
    backgroundColor: "white",
    margin: 10
  },
  heading: {
    color: "#f47b13",
    fontSize: 30
  },
  uploadAvatar: {
    height: 50,
    width: 50
  },
  submit: {
    color: "white",
    fontSize: 20,
    textAlign: "center",
    padding: 10,
    backgroundColor: "#ff6600",
    width: "40%",
    alignSelf: "flex-end"
  }
});
