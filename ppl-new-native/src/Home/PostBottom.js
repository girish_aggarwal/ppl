import React from "react";
import { TouchableHighlight, StyleSheet, View, Text } from "react-native";
import { get } from "lodash";
import axios from "axios";

export default class PostBottom extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedMail: "",
      toggle: true,
      post: {
        likes: [],
        no_of_likes: 0,
        no_of_comments: 0,
        _id: ""
      }
    };
  }

  static getDerivedStateFromProps(nextProps) {
    const { loggedMail } = nextProps;
    const { likes, comments, _id } = get(nextProps, "post");
    if (likes) {
      const index = likes.findIndex(x => x.email === loggedMail);
      let toggle = true;
      if (index >= 0) toggle = false;
      return {
        loggedMail,
        toggle,
        post: {
          likes,
          no_of_likes: likes.length,
          no_of_comments: comments.length,
          _id
        }
      };
    } else return null;
  }

  componentDidUpdate() {
    const body = {
      _id: get(this.state, "post._id"),
      likes: get(this.state, "post.likes")
    };
    axios
      .post("http://192.168.100.149:3050/timeline/updateLikes", body)
      .then(response => {
        return;
      })
      .catch(err => {
        console.log("error in postBottom axios>>", err);
      });
  }

  handleOnPress = e => {
    e.preventDefault();
    let { toggle, loggedMail } = this.state;
    let { likes, no_of_likes } = get(this.state, "post");
    if (toggle) {
      likes.push({ email: loggedMail });
      no_of_likes = no_of_likes + 1;
    } else {
      let index = likes.findIndex(email => email === loggedMail);
      likes.splice(index, 1);
      no_of_likes = no_of_likes - 1;
    }
    toggle = !toggle;
    this.setState(this.state);
  };

  render() {
    return (
      <View style={styles.main}>
        <View style={styles.button}>
          <Text style={styles.text}>
            {get(this.state, "post.no_of_likes")} Likes
          </Text>
        </View>
        <TouchableHighlight
          style={styles.button}
          underlayColor="#FAD7A0"
          onPress={this.handleOnPress}
        >
          <Text style={styles.text}>
            {this.state.toggle ? "Like" : "Unlike"}
          </Text>
        </TouchableHighlight>
        <View style={styles.button}>
          <Text style={styles.text}>
            {get(this.state, "post.no_of_comments")} Comments
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flexDirection: "row"
  },
  button: {
    padding: 5,
    width: "30%",
    backgroundColor: "#ff6600",
    margin: 5,
    alignContent: "center"
  },
  text: {
    color: "white"
  }
});
