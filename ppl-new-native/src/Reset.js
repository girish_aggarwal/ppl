import React, { Component } from "react";
import { Alert, TextInput, Button, View, Text, StyleSheet } from "react-native";
import axios from "axios";
import { set, get, unset, isEmpty } from "lodash";

class Reset extends Component {
  constructor(props) {
    super(props);
    const { email } = get(props, "navigation.state.params");
    this.state = {
      form: {
        password: "",
        cpassword: "",
        email
      },
      error: {}
    };
  }

  validateTextField = fieldName => {
    const { error, form } = this.state;
    const value = get(form, fieldName);
    let valid = false;
    valid = value.length >= 6 && value.split(" ").length === 1;

    if (valid) {
      unset(error, fieldName);
    } else {
      set(error, fieldName, `Invalid password`);
    }
    this.setState(this.state);
  };

  validateForm = () => {
    const { error, form } = this.state;
    this.validateTextField("password");
    if (get(form, "password") !== get(form, "cpassword")) {
      set(error, "mismatch", "Passwords Didn't Matched !!!");
    } else {
      unset(error, "mismatch");
    }
    return isEmpty(error);
  };

  handleOnPress = e => {
    e.preventDefault();
    if (this.validateForm()) {
      axios
        .post("http://192.168.100.149:3050/reset", get(this.state, "form"))
        .then(response => {
          return response.data;
        })
        .then(data => {
          if (data === "Changed") {
            const { navigate } = this.props.navigation;
            Alert.alert(
              "Success",
              "Password Changed Successfully, Login Again to continue ..."
            );
            navigate("Login");
          } else {
            Alert.alert("Error", "Try Again  ...");
          }
        })
        .catch(err => {
          console.log("error in fetch >>", err);
        });
    }
  };

  onChange = (fieldName, value) => {
    const { form } = this.state;
    set(form, fieldName, value);
    this.validateTextField(fieldName);
    this.setState(this.state);
  };

  showError = fieldName => {
    const { error } = this.state;
    let err = get(error, fieldName);
    if (err) {
      return <Text style={{ color: "red" }}>{err}</Text>;
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View id="1" style={styles.main}>
        <View>
          <Text style={styles.titleText}>Reset Password</Text>
          <View
            style={{
              borderBottomColor: "#f47b13",
              borderBottomWidth: 2
            }}
          />
        </View>
        <View>
          {/* <Text>E- Mail</Text> */}
          <TextInput
            style={{ height: 40 }}
            placeholder="Password"
            onChangeText={text => this.onChange("password", text)}
          />
          {this.showError("password")}
          {/* <Text>Password</Text> */}
          <TextInput
            style={{ height: 40 }}
            placeholder="Confirm Password"
            secureTextEntry={true}
            onChangeText={text => this.onChange("cpassword", text)}
          />
          {this.showError("cpassword")}
          {this.showError("mismatch")}
          <View
            style={{
              margin: 10,
              flexDirection: "row",
              alignContent: "space-around",
              justifyContent: "space-around"
            }}
          >
            <Button onPress={this.handleOnPress} title="Submit" />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fbf9f9",
    flex: 2,
    justifyContent: "space-around"
  },
  titleText: {
    color: "#f47b13",
    fontSize: 30,
    fontWeight: "bold"
  },
  button: {
    flexDirection: "row",
    justifyContent: "space-around"
  }
});

export default Reset;
