// import React, { Component } from "react";
// import { ImageBackground, View, Text, Image } from "react-native";

// class Header extends Component {
//   render() {
//     return (
//       <View
//         style={{
//           width: "100%",
//           height: "8%",
//           backgroundColor: "#ffa21d",
//           position: "relative"
//         }}
//       >
//         <View style={{ width: "30%" }}>
//           <ImageBackground
//             source={require("../public/images/logo_bg.png")}
//             style={{
//               backgroundColor: "#ffa21d",
//               width: "100%",
//               alignItems: "center",
//               height: "100%"
//             }}
//           >
//             <Image
//               style={{
//                 width: "75%",
//                 height: "80%"
//               }}
//               source={require("../public/images/logo.png")}
//             />
//           </ImageBackground>
//         </View>
//         {/*<ImageBackground source={require("../public/images/logo_bg.png")} />
//          {<Image source={require("../public/images/logo.png")} />} */}
//       </View>
//     );
//   }
// }
const Header = {
  title: "PPL Application",
  headerStyle: {
    backgroundColor: "#f4511e"
  },
  headerTintColor: "#fff",
  headerTitleStyle: {
    fontWeight: "bold"
  }
};

export default Header;
