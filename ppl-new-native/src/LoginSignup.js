import React from "react";
import { StyleSheet, View } from "react-native";
import { createStackNavigator } from "react-navigation";
import Footer from "./Footer";
import Login from "./Login";
import Signup from "./Signup";
import Forgot from "./Forgot";
import OTP from "./OTP";
import Reset from "./Reset";

const NavigationApp = createStackNavigator({
  Login: { screen: Login },
  Signup: { screen: Signup },
  Forgot: { screen: Forgot },
  OTP: { screen: OTP },
  Reset: { screen: Reset }
});

export default class LoginSignup extends React.Component {
  static navigationOptions = {
    header: null
  };

  render() {
    return (
      <View style={styles.container}>
        <NavigationApp screenProps={{ root: this.props.navigation }} />
        <Footer />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "stretch",
    justifyContent: "center",
    margin: 5
  }
});
