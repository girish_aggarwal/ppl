import React, { Component } from "react";
import { View, Text, Image } from "react-native";

class Footer extends Component {
  render() {
    return (
      <View style={{ backgroundColor: "#f4511e", padding: 5 }}>
        <View style={{ flexDirection: "row", justifyContent: "center" }}>
          <Image source={require("../public/images/social_1.png")} />
          <Image source={require("../public/images/social_2.png")} />
          <Image source={require("../public/images/social_3.png")} />
          <Image source={require("../public/images/social_4.png")} />
        </View>

        <Text style={{ color: "white", textAlign: "center" }}>
          Copyright © Pet-Social 2014
        </Text>
      </View>
    );
  }
}

export default Footer;
